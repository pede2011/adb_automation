﻿//*********************************************
//**MONKEY AUTOMATRON 5000 for Android/Amazon**
//*********************************************
//**Written by Nicolas Pedevilla on 1/10/2019**
//*********************************************

//This is to be able to use strtok and other functions without Windows flipping out
#pragma _CRT_SECURE_NO_WARNINGS
#pragma warning(disable : 4996) 

#include "pch.h"//No real reason this is here, but w/e
#include <iostream>
#include <fstream>
#include <string>
#include <tchar.h>
#include <Windows.h>

using namespace std;

string nameOfGame;
string nameCheck;
string nameOfFile = "nameOfGame.txt";

int logNumber;

//Sometime, I should make it so you can make your own custom settings. Oh well ¯\_(ツ)_/¯
string recommendedSettings = "adb shell monkey --throttle 120 --pct-touch 20 --pct-motion 20 --pct-trackball 20 --pct-nav 20 --pct-majornav 20 -p com.pixowl.tsb2 -v 5000 --ignore-timeouts --kill-process-after-error";

bool CheckGameName(const char* nameToCheck)
{
	char* gameName = (char*)nameToCheck;

	int i, num, dots = 0;
	char *ptr;

	if (gameName == NULL)
	{
		return 0;
	}

	ptr = strtok(gameName, ":");//Breaks the nameOfGame by the ':'

	if (ptr == NULL)//If there is no input
	{
		return 0;
	}

	while (ptr)
	{
		ptr = strtok(NULL, ":");
		if (ptr != NULL)
		{
			++dots;
		}
	}

	if (dots != 2)//If the name is longer than XXX:XXX:XXX
	{		
		return 0;
	}
	return 1;
}

void createNewGameEntry()
{
	cout << "Please insert the name of the game as it will appear on the log. Example: com:company:nameOfGame\n";
	cin >> nameOfGame;
	nameCheck = nameOfGame;

	while (CheckGameName(nameCheck.c_str()) != true)
	{
		cout << "INVALID NAME\n";
		cout << "Please insert the name of the game as it will appear on the log. Example: com:company:nameOfGame\n";
		cin >> nameOfGame;
		nameCheck = nameOfGame;
	}

	ofstream storeFile(nameOfFile, ofstream::out);
	storeFile << nameOfGame;
	storeFile.close();

	return;
}

void readGameEntry()
{	
	ifstream myfile(nameOfFile);
	if (myfile.is_open())
	{
		while (getline(myfile, nameOfGame))
		{
			cout << nameOfGame << '\n';
		}
		myfile.close();
	}

	else
	{
		cout << "Unable to open file. A new file will be created\n";
		createNewGameEntry();
	}

	return;
}

int setADBPath()
{
	ifstream myFile(nameOfFile);
	if (myFile.is_open() != true)
	{		
		string adbPath = string("setx adb ")+'"'+string("%path%;%LOCALAPPDATA%\Android\sdk\platform-tools")+'"';

		cout << adbPath;

		if (system(adbPath.c_str()) != true)
		{
			cout << "Added ADB to system PATH \n";
		}

		else
		{
			cout << "Unable to add ADB to system PATH. Make sure it's placed in %LOCALAPPDATA%\Android\sdk\platform-tools \n";
			return 1;
		}
	}

	else
	{
		myFile.close();
	}

	return 0;
}

/*int findADB()//The only reason this is a separate function, is so I can make it download ADB if its missing in the far far future
{
}*/

void MonkeyTouchesADBLogs()
{	
		logNumber++;
		string log = string("adb logcat>>") + to_string(logNumber) + ".txt";
		system(log.c_str());
		system(recommendedSettings.c_str());	
}

void selectADBCommand()
{
	char option;
	cout << "Press 1 to execute the recommended settings, 2 for custom:\n";
	cin >> option;

	while (option != '1' && option != '2')
	{
		cout << "INVALID INPUT\n";
		cout << "Press 1 to execute the recommended settings, 2 for custom:\n";
		cin >> option;
	}

	if (option = '1')
	{
		MonkeyTouchesADBLogs();
	}

	if (option = '2')
	{
		cout << "Whoops, not implemented";
		MonkeyTouchesADBLogs();
	}
}

int main()
{
	setADBPath();

	char option;
	cout << "Welcome to the Monkey Automatron! Enter 1 to begin testing or 2 to select a new game to test:\n";
	cin >> option;
	
	while (option != '1' && option != '2')
	{
		cout << "INVALID INPUT\n";
		cout << "Welcome to the Monkey Automatron! Enter 1 to begin testing or 2 to select a new game to test:\n";
		cin >> option;
	}

	if (option = '1')
	{
		selectADBCommand();
		readGameEntry();
	}
	
	if (option = '2')
	{
		createNewGameEntry();
		readGameEntry();
		selectADBCommand();
	}

	system("PAUSE");
	return 0;
}